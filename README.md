# What is this:
This is an Arma scripting (.SQF & .SQM) syntax highlighter to be used in Notepad++.  
At the moment of writing this is the Syntax Highlighter with the most complete and up-to-date database.  
  
# It comes in 2 flavors:  
- A **BLACK edition** especially designed for dark colored backgrounds. [(DOWNLOAD)](https://bitbucket.org/kamari/sqf-syntax-highlighter-npp/downloads)  
- A **WHITE edition** especially designed for light colored backgrounds. (Under construction)   
  
# Installation:  
* Download the latest version and unpack the file: 'userDefineLang.xml'   
* Copy the file into: **C:\Users\[user]\AppData\Roaming\Notepad++\**   
* Open your .SQF scripting file.  
* It should auto-detect the correct language for your file, if not follow the step below:  
* Select **'Ahoyworld_Arma3'** from the **Language menu** in NPP  
  
# Links:  
* [F.A.Q.](https://bitbucket.org/kamaradski/sqf-syntax-highlighter-npp/wiki/Frequently%20Asked%20Questions)  
* [WIKI](https://bitbucket.org/kamaradski/sqf-syntax-highlighter-npp/wiki/Home)  
  
* [Ahoyworld release](http://www.ahoyworld.co.uk/topic/3135-ahoyworld-notepad-arma-syntax-highlighter/)  
* [BIS forum release](http://forums.bistudio.com/showthread.php?180720-AhoyWorld-Notepad-Arma-Syntax-Highlighter)  
* [Developer Homepage](http://kamaradski.com)  
  
# Bittorrent Sync:  
Black-Edition - BPACC6NL7I37OWYL4MZCHMNPJMUFOJOCP  
White-Edition - BQ4NAI4LFVKM6LNU3VMJJA5H7PJLMGAKW
  
  
**Please note:** This is a community project, and we will appreciate (and a little bit depend on,) your help to keep this updated and working.   
Submit all your input (or questions) on the **[Issue Tracker](https://bitbucket.org/kamaradski/sqf-syntax-highlighter-npp/issues?status=new&status=open)** so we can implement your suggestions.  
   
   
![NPP_SH.png](https://bitbucket.org/repo/4Az5ne/images/805320600-NPP_SH.png)  
  
  
### Contact:  
* kamaradski at hotmail dot com or PM me on www.ahoyworld.co.uk